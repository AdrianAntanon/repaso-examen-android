package com.example.repasoexamenroom.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.repasoexamenroom.Database.Stat;
import com.example.repasoexamenroom.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Stat> stats;
    private final int layout;

    public MyAdapter(List<Stat> stats, int layout) {
        this.stats = stats;
        this.layout = layout;
    }


    /*IMPORTANTE, siempre tenemos que notificar el notifyDataSetChanged()
    para que registre y refresque los datos de la app*/
    public void setStats(List<Stat> stats){
        this.stats = stats;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindStats(this.stats.get(position));
    }

    @Override
    public int getItemCount() {
        return this.stats.size();
    }

    //    Esto será lo primero a crear una vez hagamos que herede de ello la clase principal, es para no tener problemas
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemViewName, itemViewStats;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemViewName = itemView.findViewById(R.id.itemViewName);
            this.itemViewStats = itemView.findViewById(R.id.itemViewStats);
        }

        public void bindStats(Stat stats) {
            this.itemViewName.setText(stats.getPlayerName());

            String playerStats = "";

            if (stats.getStat() == 1) {
                playerStats = stats.getStat() + " punto";
            } else {
                playerStats = stats.getStat() + " puntos";
            }

            this.itemViewStats.setText(playerStats);
        }
    }
}
