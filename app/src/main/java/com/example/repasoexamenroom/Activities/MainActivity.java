package com.example.repasoexamenroom.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.repasoexamenroom.Database.AppDataBase;
import com.example.repasoexamenroom.Database.DAOQuestion;
import com.example.repasoexamenroom.Database.DAOStat;
import com.example.repasoexamenroom.Database.Question;
import com.example.repasoexamenroom.Database.Stat;
import com.example.repasoexamenroom.R;
import com.example.repasoexamenroom.Repositories.QuestionRepository;
import com.example.repasoexamenroom.Repositories.StatRepository;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    static List<Question> questions;
    static List<Stat> stats;

    static AppDataBase dataBase;
    static DAOStat daoStat;
    static DAOQuestion daoQuestion;
    static QuestionRepository questionRepository;
    static StatRepository statRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startGame = findViewById(R.id.startGame);
        Button gameStats = findViewById(R.id.gameStats);

        dataBase = AppDataBase.getINSTANCE(this);

        daoStat = dataBase.daoStat();
        statRepository = new StatRepository(daoStat);
        stats = statRepository.getAllStats();

        daoQuestion = dataBase.daoQuestion();
        questionRepository = new QuestionRepository(daoQuestion);
        loadQuestions();
        questions = questionRepository.getAllQuestions();

//        Añadimos los clickListener para pasar mediante intent a las siguientes pantallas y poder interactuar


        gameStats.setOnClickListener(v -> {
            if (stats.size() > 0){
                Intent intent = new Intent(MainActivity.this, StatsActivity.class);
                startActivity(intent);
            }else {
                Toast.makeText(getApplicationContext(),"Todavía no hay puntuaciones disponibles",Toast.LENGTH_SHORT).show();
            }
        });

        startGame.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        stats = statRepository.getAllStats();
    }

    public void loadQuestions(){
        questionRepository.insertQuestion(new Question("¿Grupo de metal industrial más conocido del mundo?","Rammstein"));
        questionRepository.insertQuestion(new Question("¿Cómo se llama el cantante de Iron Maiden?","Bruce Dickinson"));
        questionRepository.insertQuestion(new Question("¿Qué grupo es considerado el creador del género metal?","Black Sabbath"));
        questionRepository.insertQuestion(new Question("¿Grupo más famoso del Big Four?","Metallica"));
        questionRepository.insertQuestion(new Question("¿De qué país son originarios los hermanos 'Young' de AC/DC?","Australia"));
        questionRepository.insertQuestion(new Question("En la película ‘Ace Ventura: Detective de Animales’, ¿qué grupo hizo una pequeña aparición?","Cannibal Corpse"));
        questionRepository.insertQuestion(new Question("Grupo de black metal responsable de la quema de iglesias en Noruega","Mayhem"));
        questionRepository.insertQuestion(new Question("La mascota del grupo es conocida como Vic Rattlehead, ¿de qué grupo hablamos? ","Megadeth"));
        questionRepository.insertQuestion(new Question("Nombre del cantante de Mötorhead","Lemmy Kilmister"));
        questionRepository.insertQuestion(new Question("Mötorhead, Megadeth, Metallica, Slayer, uno de ellos no es del big four, ¿qué grupo?","Mötorhead"));
    }
}