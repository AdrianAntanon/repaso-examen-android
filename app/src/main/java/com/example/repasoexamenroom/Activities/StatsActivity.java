package com.example.repasoexamenroom.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.repasoexamenroom.Adapters.MyAdapter;
import com.example.repasoexamenroom.Database.Stat;
import com.example.repasoexamenroom.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class StatsActivity extends AppCompatActivity {

    private MyAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_stats);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        FloatingActionButton cleanButton = findViewById(R.id.floatingActionButton);

        List<Stat> sortStats = MainActivity.statRepository.getAllStats();
        adapter = new MyAdapter(sortStats, R.layout.item_view);

        recyclerView.setAdapter(adapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        cleanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.statRepository.deleteAllStats();
                MainActivity.stats = MainActivity.statRepository.getAllStats();
                adapter.setStats(MainActivity.stats);
//
//                if (MainActivity.stats.size() == 0){
//                    finish();
//                }

                finish();
//

            }
        });
    }
}
