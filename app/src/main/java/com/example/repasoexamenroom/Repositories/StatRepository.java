package com.example.repasoexamenroom.Repositories;

import com.example.repasoexamenroom.Database.DAOStat;
import com.example.repasoexamenroom.Database.Stat;

import java.util.List;

public class StatRepository {
    DAOStat daoStat;

    public StatRepository(DAOStat daoStat) {
        this.daoStat = daoStat;
    }

    public void insertStat(Stat stat){
        daoStat.insertStat(stat);
    }

    public List<Stat> sortStats(){
        return daoStat.sortStats();
    }

    public void updateStats(Stat stat){
        daoStat.updateStats(stat);
    }

    public List<Stat> getAllStats(){
        return daoStat.getAllStats();
    }

    public Stat findById(int id){
        return daoStat.findById(id);
    }

    public void deleteAllStats(){
        daoStat.deleteAllStats();
    }


}
