package com.example.repasoexamenroom.Repositories;

import com.example.repasoexamenroom.Database.DAOQuestion;
import com.example.repasoexamenroom.Database.Question;

import java.util.List;

public class QuestionRepository {
    DAOQuestion daoQuestion;

    public QuestionRepository(DAOQuestion daoQuestion) {
        this.daoQuestion = daoQuestion;
    }

    public Question findById(int id){
        return daoQuestion.findById(id);
    }

    public void insertQuestion(Question question){
        daoQuestion.insertQuestion(question);
    }

    public void deleteAllQuestions(){
        daoQuestion.deleteAllQuestions();
    }

    public List<Question> getAllQuestions(){
        return daoQuestion.getAllQuestions();
    }


}
