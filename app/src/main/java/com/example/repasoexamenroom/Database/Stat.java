package com.example.repasoexamenroom.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Stat {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "stat_id")
    private int idStat;
    private int stat;
    private String playerName;

    public Stat(int stat, String playerName) {
        this.stat = stat;
        this.playerName = playerName;
    }

    public int getIdStat() {
        return idStat;
    }

    public void setIdStat(int idStat) {
        this.idStat = idStat;
    }

    public int getStat() {
        return stat;
    }

    public void setStat(int stat) {
        this.stat = stat;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
