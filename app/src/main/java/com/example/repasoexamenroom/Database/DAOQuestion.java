package com.example.repasoexamenroom.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DAOQuestion {
    @Query("SELECT * FROM Question WHERE question_id == :id")
    public Question findById(int id);

    @Query("SELECT * FROM Question")
    public List<Question> getAllQuestions();

    @Query("DELETE FROM Question")
    public void deleteAllQuestions();

    @Insert
    public void insertQuestion(Question question);
}
