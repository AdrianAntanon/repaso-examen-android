package com.example.repasoexamenroom.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DAOStat {
    @Insert
    public void insertStat(Stat stat);

    @Update
    public void updateStats(Stat stat);

    @Query("SELECT * FROM Stat")
    public List<Stat> getAllStats();

    @Query("SELECT * FROM Stat ORDER BY stat DESC")
    public List<Stat> sortStats();

    @Query("SELECT * FROM Stat WHERE stat_id == :id")
    public Stat findById(int id);

    @Query("DELETE FROM Stat")
    public void deleteAllStats();
}
