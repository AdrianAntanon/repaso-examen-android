package com.example.repasoexamenroom.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Question.class, Stat.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {
    public static AppDataBase INSTANCE;

    public abstract DAOQuestion daoQuestion();
    public abstract DAOStat daoStat();

    public static AppDataBase getINSTANCE(Context context) {
        if (INSTANCE==null){
            INSTANCE = Room.databaseBuilder(context,AppDataBase.class,"Trivial.db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
